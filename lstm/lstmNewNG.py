import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas import datetime
import math, time
import itertools
from sklearn import preprocessing
import datetime
from operator import itemgetter
from sklearn.metrics import mean_squared_error
from math import sqrt
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.recurrent import LSTM
from keras.models import load_model
import keras
import h5py
import requests
import os


def Wholefn(dirname,filname,trainSplit,dropoutValue,layersValue,numberofNeurons,numberofNeuronsInDense,activationValue1,activationValue2,lossValue,optimizerValue,metricsValue,kernel_initializer_value,windowValue,batch_size_value,epochs_value,validation_split_value):

    directory = dirname
    filename = filname
    df = pd.read_csv(directory+filename, index_col = 0)
    df["adj close"] = df.Close # Moving close to the last column
    df.drop(['Close'], 1, inplace=True) # Moving close to the last column
    df.drop(['OpenInt'],1,inplace=True)
    df.head()
    



    def normalize_data(df):
        min_max_scaler = preprocessing.MinMaxScaler()
        df['Open'] = min_max_scaler.fit_transform(df.Open.values.reshape(-1,1))
        df['High'] = min_max_scaler.fit_transform(df.High.values.reshape(-1,1))
        df['Low'] = min_max_scaler.fit_transform(df.Low.values.reshape(-1,1))
        df['Volume'] = min_max_scaler.fit_transform(df.Volume.values.reshape(-1,1))
        df['adj close'] = min_max_scaler.fit_transform(df['adj close'].values.reshape(-1,1))
        return df
    
    df = normalize_data(df)
    
    print(df)
    def load_data(stock, seq_len):
        amount_of_features = len(stock.columns) # 5
        data = stock.as_matrix() 
        sequence_length = seq_len + 1 # index starting from 0
        result = []
        
        for index in range(len(data) - sequence_length): # maxmimum date = lastest date - sequence length
            result.append(data[index: index + sequence_length]) # index : index + 22days
        
        result = np.array(result)
        row = round(trainSplit * result.shape[0]) # 90% split
        train = result[:int(row), :] # 90% date, all features 
        

        x_train = train[:, :-1] 
        y_train = train[:, -1][:,-1]
        x_test = result[int(row):, :-1] 
        y_test = result[int(row):, -1][:,-1]

        x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], amount_of_features))
        x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], amount_of_features))  

        return [x_train, y_train, x_test, y_test]

        
    
    def build_model(layers,layersValue):
        d = dropoutValue
        if layersValue==1:

            model = Sequential()
            
                
            model.add(LSTM(numberofNeurons, input_shape=(layers[1], layers[0]), return_sequences=False))
            model.add(Dropout(d))
                
            model.add(Dense(numberofNeuronsInDense,kernel_initializer=kernel_initializer_value,activation=activationValue1))        
            model.add(Dense(1,kernel_initializer=kernel_initializer_value,activation=activationValue2))

        elif layersValue == 2:

            model = Sequential()
            
            model.add(LSTM(numberofNeurons, input_shape=(layers[1], layers[0]), return_sequences=True))
            model.add(Dropout(d))
                
            model.add(LSTM(numberofNeurons, input_shape=(layers[1], layers[0]), return_sequences=False))
            model.add(Dropout(d))
                
            model.add(Dense(numberofNeuronsInDense,kernel_initializer=kernel_initializer_value,activation=activationValue1))        
            model.add(Dense(1,kernel_initializer=kernel_initializer_value,activation=activationValue2))
        
        elif layersValue == 3:
            model = Sequential()
            
            model.add(LSTM(numberofNeurons, input_shape=(layers[1], layers[0]), return_sequences=True))
            model.add(Dropout(d))
                
            model.add(LSTM(numberofNeurons, input_shape=(layers[1], layers[0]), return_sequences=True))
            model.add(Dropout(d))
                
            model.add(LSTM(numberofNeurons, input_shape=(layers[1], layers[0]), return_sequences=False))
            model.add(Dropout(d))

            model.add(Dense(numberofNeuronsInDense,kernel_initializer=kernel_initializer_value,activation=activationValue1))        
            model.add(Dense(1,kernel_initializer=kernel_initializer_value,activation="linear"))    

            
        start = time.time()
        model.compile(loss=lossValue,optimizer=optimizerValue, metrics=['accuracy'])
        print("Compilation Time : ", time.time() - start)
        return model

    window = windowValue
    X_train, y_train, X_test, y_test = load_data(df, window)


    model = build_model([5,window,1],layersValue)


    model.fit(X_train,y_train,batch_size=batch_size_value,epochs=epochs_value,validation_split=validation_split_value,verbose=1,shuffle=False)


    diff=[]
    ratio=[]


    p = model.predict(X_test)


    df = pd.read_csv(directory + filename, index_col = 0)
    df["adj close"] = df.Close # Moving close to the last column
    df.drop(['Close'], 1, inplace=True) # Moving close to the last column
    df.drop(['OpenInt'],1,inplace=True)


    def denormalize(df, normalized_value): 

        df = df['adj close'].values.reshape(-1,1)
        normalized_value = normalized_value.reshape(-1,1)
        min_max_scaler = preprocessing.MinMaxScaler()
        a = min_max_scaler.fit_transform(df)
        new = min_max_scaler.inverse_transform(normalized_value)
        return new

    newp = denormalize(df, p)
    newy_test = denormalize(df, y_test)


    def model_score(model, X_train, y_train, X_test, y_test):
        trainScore = model.evaluate(X_train, y_train, verbose=1)
        print('Train Score: %.5f MSE (%.2f RMSE)' % (trainScore[0], math.sqrt(trainScore[0])))
        testScore = model.evaluate(X_test, y_test, verbose=1)
        print('Test Score: %.5f MSE (%.2f RMSE)' % (testScore[0], math.sqrt(testScore[0])))
        return [trainScore[0],math.sqrt(trainScore[0]),testScore[0],math.sqrt(testScore[0])]

    return model_score(model, X_train, y_train, X_test, y_test)






def myTweakingFn(HyperparametersArray,parameterToBeTweaked,ArrayOfParameterArray):
    finalrmse = 1000
    indexval = 200
    for j in range(len(ArrayOfParameterArray[parameterToBeTweaked])):
    
        HyperparametersArray[parameterToBeTweaked] = ArrayOfParameterArray[parameterToBeTweaked][j]
        avgtestrmse = 0
       
        trainmset,trainrmset,testmset,testrmset = Wholefn(HyperparametersArray[0],HyperparametersArray[1],HyperparametersArray[2],HyperparametersArray[3],HyperparametersArray[4],HyperparametersArray[5],HyperparametersArray[6],HyperparametersArray[7],HyperparametersArray[8],HyperparametersArray[9],HyperparametersArray[10],HyperparametersArray[11],HyperparametersArray[12],HyperparametersArray[13],HyperparametersArray[14],HyperparametersArray[15] ,HyperparametersArray[16]  )
        print("\nHyperparametersArray:-\n",HyperparametersArray)
        print("\ntrainmset,trainrmset,testmset,testrmset:-\n",trainmset,trainrmset,testmset,testrmset)
    return [HyperparametersArray,avgtestrmse]

dirname = '/home/uttam/uttam/FYP/Data/Data/Stocks/' 
filname = 'dhg.us.txt'
trainSplit = 0.9
dropoutValue = 0.3
layersValue = 3
numberofNeurons = 256
numberofNeuronsInDense = 32
activationValue1 = 'sigmoid'
activationValue2 = 'linear'
lossValue = 'mse'
optimizerValue = 'adam'
metricsValue = [['accuracy']]
kernel_initializer_value = "uniform"
windowValue = 1  
batch_size_value = 32
epochs_value = 10
validation_split_value = 0.2



HyperparametersArray = [dirname,filname,trainSplit,dropoutValue,layersValue,numberofNeurons,numberofNeuronsInDense,activationValue1,activationValue2,lossValue,optimizerValue,metricsValue,kernel_initializer_value,windowValue,batch_size_value,epochs_value,validation_split_value]
ArrayOfParameterArray = [['/home/uttam/uttam/FYP/Data/Data/Stocks/' ],['dhg.us.txt','gjp.us.txt','goog.us.txt','googl.us.txt','ccl.us.txt','ecl.us.txt','see.us.txt','aa.us.txt','ibm.us.txt','jpm.us.txt'],[0.7,0.8,0.9],[0.1,0.2,0.3],[1,2,3],[16,64,256],[16,32],['relu'],['linear'],['mse'],['adam'],[['accuracy']],["uniform"],[1],[1,16,64,256,512],[5,10],[0.1,0.2,0.3]]
n = len(HyperparametersArray)
trainmset,trainrmset,testmset,testrmset = Wholefn(HyperparametersArray[0],HyperparametersArray[1],HyperparametersArray[2],HyperparametersArray[3],HyperparametersArray[4],HyperparametersArray[5],HyperparametersArray[6],HyperparametersArray[7],HyperparametersArray[8],HyperparametersArray[9],HyperparametersArray[10],HyperparametersArray[11],HyperparametersArray[12],HyperparametersArray[13],HyperparametersArray[14],HyperparametersArray[15] ,HyperparametersArray[16]  )
print("\nHyperparametersArray:-\n",HyperparametersArray)
print("\ntrainmset,trainrmset,testmset,testrmset:-\n",trainmset,trainrmset,testmset,testrmset)
